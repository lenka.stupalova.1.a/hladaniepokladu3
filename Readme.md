******************************
**** HELP - Spustenie hry ****
******************************

Spustenie hry z cmd:

Stiahnite, rozbalte.

Presunte sa do zlozky \src\main\java

Spustite kompilaciu: javac HladaniePokladu.java

Hru teraz mozete spustat pomocou prikazu: java HladaniePokladu

Postupujte podla instrukcii v hre!

V tejto zlozke je prilozeny aj plan s ostrovami - Plan_K_Hre_Hladanie_Pokladu.png

Pre Readme s diakritikou otvorte subor Readme.txt

******************************
****** INFO - Popis hry ******
******************************

Vitajte vo svete piratov, hrozivych zralokov a blyskajuceho sa pokladu!

V tejto command line textovej hre, Hladanie Pokladu, sa mozete zahrat na pirata, 
plavit sa medzi ostrovami a skusit si ake je to najst poklad. Ale pozor, ak sa 
dostanete na ostrov, z ktoreho uz niet uniku, zozeru vas skor ci neskor nelutostne 
zraloky!

V hre zacinate na policku Start. Nasledne vam hra ponukne ostrovy, na ktore sa da 
z ostrovu, na ktorom prave stojite, dostat. Vy si vyberiete, na ktory ostrov 
z ponuky chcete ist zadanim cisla ostrovu. Na tento vybrany ostrov sa potom presuniete. 
Nasledne hra opat vypise ponuku ostrovov, na ktore sa da dostat dalej. 
Hrac si opat vyberie kam chce ist a "preplavi" sa na tento ostrov.
Hra pokracuje az pokym sa hrac nedostane na ostrov, z ktoreho sa uz neda 
plavit nikam alebo na ostrov s pokladom.
Hra taktiez konci ak hrac zada neplatny vstup alebo zada cislo ostrova, 
ktory nema aktualne v ponuke.

Hra ma predom urceny "plan hry", ma urcene pozicie ostrovov aj s urcenymi moznostami 
presunu medzi nimi. 

Vela stastia pri hladani pokladu!