import java.util.ArrayList;
import java.util.List;

public class VytvaracOstrovov {

	/**
	 * @return
	 * Vytvori ostrovy
	 */
	public static Ostrov vyvtorOstrovy() {
		List<Ostrov> vec = new ArrayList<Ostrov>();

		for (int i=0; i< 15;i++) {
			final int q = i;
			Ostrov o = new Ostrov(q);
			vec.add(o);
			if(i==14) { //ostrov 14 je ciel
				o.urobTotoKoniec();
			}
		}
		
		vec.get(0).pridajOstrov(vec.get(1));
		vec.get(0).pridajOstrov(vec.get(8));
		
		vec.get(1).pridajOstrov(vec.get(2));
		
		vec.get(2).pridajOstrov(vec.get(8));
		vec.get(2).pridajOstrov(vec.get(3));
		
		vec.get(3).pridajOstrov(vec.get(2));
		vec.get(3).pridajOstrov(vec.get(4));
		
		vec.get(4).pridajOstrov(vec.get(5));
		
		vec.get(5).pridajOstrov(vec.get(8));
		vec.get(5).pridajOstrov(vec.get(6));
		vec.get(5).pridajOstrov(vec.get(10));
		
		vec.get(6).pridajOstrov(vec.get(5));
		vec.get(6).pridajOstrov(vec.get(7));

		vec.get(7).pridajOstrov(vec.get(6));
		vec.get(7).pridajOstrov(vec.get(10));
		vec.get(7).pridajOstrov(vec.get(11));
		
		vec.get(8).pridajOstrov(vec.get(2));
		vec.get(8).pridajOstrov(vec.get(5));
		vec.get(8).pridajOstrov(vec.get(9));
		
		vec.get(9).pridajOstrov(vec.get(13));
		
		vec.get(10).pridajOstrov(vec.get(7));
		vec.get(10).pridajOstrov(vec.get(14));
		
		vec.get(11).pridajOstrov(vec.get(7));
		vec.get(11).pridajOstrov(vec.get(12));
		
		vec.get(12).pridajOstrov(vec.get(11));
		
		return vec.get(0);
		
	}
}
