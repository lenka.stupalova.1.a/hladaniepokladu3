
/**
 * @author CGI
 * Trieda obsahuje metody na zistenie roznych podmienok a testuje korektnost parametrov.
 */
public class Testy {
	/**
	 * @param s
	 * @return Zisti ci zadany retazec je integer v 10-kovej sustave
	 */
	public static boolean isInteger(String s) {
		return isInteger(s, 10);
	}

	/**
	 * @param s, r
	 * @return Zisti ci zadany retazec je integer v danej sustave
	 */
	public static boolean isInteger(String s, int radix) {
		if (s.isEmpty())
			return false;
		for (int i = 0; i < s.length(); i++) {
			if (i == 0 && s.charAt(i) == '-') {
				if (s.length() == 1)
					return false;
				else
					continue;
			}
			if (Character.digit(s.charAt(i), radix) < 0)
				return false;
		}
		return true;
	}

	/**
	 * @param dalej
	 * @return Zisti ci pouzivatel chce skoncit a ci zadal ciselny retazec.
	 */
	public static boolean jeDobryStringOver(String dalej) {

		if (dalej.equals("Koniec") || dalej.equals("koniec") || dalej.equals("End") || dalej.equals("q")) {
			System.out.println("Zvolili ste koniec hry.");
			return false;
		}
		if (!Testy.isInteger(dalej)) {
			System.out.println("Neplatny vstup. Koncim pre podozrenie z podvadzania.");
			return false;
		}
		return true;
	}
}
