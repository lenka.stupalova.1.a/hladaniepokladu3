import java.util.ArrayList;
import java.util.List;

public class Ostrov {

	private int mojeid;
	private List<Ostrov> kam = new ArrayList<Ostrov>();
	private boolean somKoniec = false;
	public boolean daSaIstDalej;

	public Ostrov() {
		this.mojeid = 0;
	}

	public Ostrov(int id) {
		this.mojeid = id;
	}

	/**
	 * @param o
	 * Prida ostrov o do zoznamu ostrovov, na ktore sa da prejst z tohto ostrova.
	 *  
	 */
	public void pridajOstrov(Ostrov o) {
		this.kam.add(o);
	}

	/**
	 * Nastavi tento ostrov ako cielovy
	 */
	public void urobTotoKoniec() {
		this.somKoniec = true;
	}

	@Override
	public String toString() {
		return "Ostrov [mojeid=" + mojeid + ", somKoniec=" + somKoniec + "]";
	}

	/**
	 * Vypise svoje id a moznosti ostrovov na ktore sa da prejst z tohto ostrova
	 */
	public void vypisSebaAMoznosti() {
		System.out.println("Ste na Ostrove " + this.mojeid);
		System.out.println("Dalej sa mozete presunut na ostrovy:");
		for (int i = 0; i < this.kam.size(); i++) {
			System.out.println("    Ostrov " + kam.get(i).mojeid);
		}
		System.out.println("Zadajte cislo ostrovu na ktory sa chcete presunut:");

	}

	/**
	 * @return Vrati ci sa da ist z tohto ostrova dalej
	 * Ak je prazdny list kam, tak sa z tohto ostrova uz neda ist nika dalej
	 * 
	 */
	public boolean daSaIstDalej() {
		if (kam.size() == 0) {
			return false;
		}
		return true;
	}

	/**
	 * @param chcemIst
	 * @return
	 * zisti ci je mozne prejst na zadany ostrov s id chcemIst a vrati tento ostrov
	 */
	public Ostrov prechodNaOstrov(Integer chcemIst) {
		int id = -1;
		for (int i = 0; i < kam.size(); i++) {
			if(kam.get(i).getMojeid() == chcemIst) {
				id = i;
			}
		}

		if (id == -1) {
			return null;
		}
		else {
			return kam.get(id);
		}
	}

	public int getMojeid() {
		return mojeid;
	}

	public void setMojeid(int mojeid) {
		this.mojeid = mojeid;
	}

	public boolean isSomKoniec() {
		return somKoniec;
	}

	public void setSomKoniec(boolean somKoniec) {
		this.somKoniec = somKoniec;
	}
	
	

}
