******************************
**** HELP - Spustenie hry ****
******************************

Spustenie hry z cmd:

Stiahnite, rozbalte.
Presu�te sa do zlo�ky \src\main\java
Spustite kompil�ciu: javac HladaniePokladu.java
Hru teraz m��ete sp���a� pomocou pr�kazu: java HladaniePokladu

Postupujte pod�a in�trukcii v hre!

V tejto zlo�ke je prilo�en� aj pl�n s ostrovami - Plan_K_Hre_Hladanie_Pokladu.png


******************************
****** INFO - Popis hry ******
******************************

Vitajte vo svete pir�tov, hroziv�ch �ralokov a bl�skaj�ceho sa pokladu!

V tejto command line textovej hre, Hladanie Pokladu, sa m��ete zahra� na pir�ta, 
plavi� sa medzi ostrovami a sk�si� si ak� je to n�js� poklad. Ale pozor, ak sa 
dostanete na ostrov, z ktor�ho u� niet �niku, zo�er� v�s sk�r �i nesk�r ne��tostn� 
�raloky!

V hre za��nate na pol��ku �tart. N�sledne v�m hra pon�kne ostrovy, na ktor� sa d� 
z ostrovu, na ktorom pr�ve stoj�te, dosta�. Vy si vyberiete, na ktor� ostrov 
z ponuky chcete �s� zadan�m ��sla ostrovu. Na tento vybran� ostrov sa potom presuniete. 
N�sledne hra op� vyp�e ponuku ostrovov, na ktor� sa d� dosta� �alej. 
Hr�� si op� vyberie kam chce �s� a "preplav�" sa na tento ostrov.
Hra pokra�uje a� pok�m sa hr�� nedostane na ostrov, z ktor�ho sa u� ned� 
plavi� nikam alebo na ostrov s pokladom.
Hra taktie� kon�� ke� hr�� zad� neplatn� vstup alebo zad� ��slo ostrova, 
ktor� nem� aktu�lne v ponuke.

Hra m� predom ur�en� "pl�n hry", m� ur�en� poz�cie ostrovov aj s ur�en�mi mo�nos�ami 
presunu medzi nimi. 

Ve�a ��astia pri h�adan� pokladu!